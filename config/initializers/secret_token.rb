# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Blog::Application.config.secret_key_base = 'b1eab567ceac5a6f4dc4be0f9693c466e36de06f684e2ed721cff8617013ccd2c97f308a6f7dc9b860f7c1ec06bce0fb2182b69f768587c531f9bdcca7114110'
